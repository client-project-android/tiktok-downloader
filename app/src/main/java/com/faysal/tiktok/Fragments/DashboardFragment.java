package com.faysal.tiktok.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.downloader.Constants;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdIconView;
import com.facebook.ads.AdOptionsView;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.facebook.ads.NativeBannerAd;
import com.faysal.tiktok.R;
import com.faysal.tiktok.Utils.CommonMethods;
import com.faysal.tiktok.myapp.MainActivity;
import com.google.android.material.snackbar.Snackbar;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.nibor.autolink.LinkExtractor;
import org.nibor.autolink.LinkSpan;
import org.nibor.autolink.LinkType;

import static android.content.Context.CLIPBOARD_SERVICE;

public class DashboardFragment extends Fragment implements OnClickListener {
    private static boolean isAPrimeMember;
    private final String TAG = DashboardFragment.class.getSimpleName();
    /* access modifiers changed from: private */
    public Activity context;
    private DownloadFileFromURL downloadFileFromURLTask;
    /* access modifiers changed from: private */
    public int downloadId;
    String download_url = null;
    private Dialog downloadingDialog;
    private Handler handler = new Handler();
    String newVideoURL;
    private ProgressDialog progressDialog;
    private int progressStatus = 0;
    /* access modifiers changed from: private */
    public TextView progressText;
    RequestQueue queue1;
    private View rootView;
    private EditText searchText;
    CircularProgressBar circularProgressBar;

    private NativeAdLayout nativeAdLayout;
    private LinearLayout adView;
    private NativeAd nativeAd;

    class ProgressListners implements OnProgressListener {
        ProgressListners() {
        }

        public void onProgress(Progress progress) {

            long j = (progress.currentBytes * 100) / progress.totalBytes;
            TextView access$200 = DashboardFragment.this.progressText;

            circularProgressBar =DashboardFragment.this.circularProgressBar;
            StringBuilder sb = new StringBuilder();
            sb.append(j);
            sb.append("%");
            access$200.setText(sb.toString());
            circularProgressBar.setVisibility(View.VISIBLE);
            circularProgressBar.setProgress(Integer.parseInt(String.valueOf(j)));
        }
    }

    class progressClass implements OnCancelListener {
        progressClass() {
        }

        public void onCancel() {
            DashboardFragment.this.downloadId = 0;
            MainActivity.pager.setCurrentItem(0);
        }
    }

    class VidPlayer implements OnPauseListener {
        VidPlayer() {
        }

        public void onPause() {
            PRDownloader.resume(DashboardFragment.this.downloadId);
            Toast.makeText(DashboardFragment.this.context, "Download Paused", Toast.LENGTH_SHORT).show();
        }
    }

    class Players implements OnStartOrResumeListener {
        Players() {
        }

        public void onStartOrResume() {
            MainActivity.pager.setCurrentItem(1);
        }
    }

    @SuppressLint({"StaticFieldLeak"})
    private class DownloadFileFromURL extends AsyncTask<String, String, String> {
        String URL;

        class C28011 implements Listener<String> {
            C28011() {
            }

            public void onResponse(String str) {
                PrintStream printStream = System.out;
                StringBuilder sb = new StringBuilder();
                sb.append("responseeeeeee=");
                sb.append(str);
                printStream.println(sb.toString());
                String[] split = str.split("play_addr\"");
                PrintStream printStream2 = System.out;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("a=");
                sb2.append(split.length);
                printStream2.println(sb2.toString());
                PrintStream printStream3 = System.out;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("a1=");
                sb3.append(split[0]);
                printStream3.println(sb3.toString());
                PrintStream printStream4 = System.out;
                StringBuilder sb4 = new StringBuilder();
                sb4.append("2=");
                sb4.append(split[1]);
                printStream4.println(sb4.toString());
                String[] split2 = split[1].split("https");
                PrintStream printStream5 = System.out;
                StringBuilder sb5 = new StringBuilder();
                sb5.append("b=");
                sb5.append(split2[0]);
                printStream5.println(sb5.toString());
                PrintStream printStream6 = System.out;
                StringBuilder sb6 = new StringBuilder();
                sb6.append("b1=");
                sb6.append(split2[1]);
                printStream6.println(sb6.toString());
                String[] split3 = split2[1].split("\"");
                PrintStream printStream7 = System.out;
                StringBuilder sb7 = new StringBuilder();
                sb7.append("c=");
                sb7.append(split3[0]);
                printStream7.println(sb7.toString());
                DashboardFragment dashboardFragment = DashboardFragment.this;
                StringBuilder sb8 = new StringBuilder();
                sb8.append("https");
                sb8.append(split3[0]);
                dashboardFragment.download_url = sb8.toString();
                StringBuilder sb9 = new StringBuilder();
                sb9.append("urllllllllllllllll=");
                sb9.append(DashboardFragment.this.download_url);
                printStream7.println(sb9.toString());
                DashboardFragment.this.downloadMedia(DashboardFragment.this.download_url);
            }
        }

        class C28022 implements ErrorListener {
            C28022() {
            }

            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(DashboardFragment.this.getActivity(), "11111111111111111", Toast.LENGTH_LONG).show();
                if (volleyError instanceof NetworkError) {
                    Toast.makeText(DashboardFragment.this.getActivity(), "Can't connect to Internet. Please check your connection.", Toast.LENGTH_LONG).show();
                } else if (volleyError instanceof ServerError) {
                    Toast.makeText(DashboardFragment.this.getActivity(), "Unable to login. Either the username or password is incorrect.", Toast.LENGTH_LONG).show();
                } else if (volleyError instanceof ParseError) {
                    Toast.makeText(DashboardFragment.this.getActivity(), "Parsing error. Please try again.", Toast.LENGTH_LONG).show();
                } else if (volleyError instanceof NoConnectionError) {
                    Toast.makeText(DashboardFragment.this.getActivity(), "Can't connect to internet. Please check your connection.", Toast.LENGTH_LONG).show();
                } else if (volleyError instanceof TimeoutError) {
                    Toast.makeText(DashboardFragment.this.getActivity(), "Connection timed out. Please check your internet connection.", Toast.LENGTH_LONG).show();
                }
                volleyError.printStackTrace();
                StringBuilder sb = new StringBuilder();
                sb.append("error => ");
                sb.append(volleyError.toString());
            }
        }

        public DownloadFileFromURL(String str) {
            this.URL = str;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            DashboardFragment.this.showProgressDialog();
        }

        /* access modifiers changed from: protected */
        public final String doInBackground(String... strArr) {
            String str = "";
            try {
                Document document = Jsoup.connect(this.URL).ignoreContentType(true).userAgent("Mozilla/5.0 (Linux; U; Android 4.1.1; en-us; Samsung Galaxy S2 - 4.1.1 - API 16 - 480x800 Build/JRO03S) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30").header("Content-Type", new UnsupportedMimeTypeException("Hey this is Mime", "application/json", "http://dictionary.cambridge.org/dictionary/english/reality").getMimeType()).ignoreContentType(true).get();
                PrintStream printStream = System.out;
                StringBuilder sb = new StringBuilder();
                sb.append("Document hhhhhhhhhhhhhhhhhh ");
                sb.append(document.toString());
                printStream.println(sb.toString());
                String[] split = document.toString().split("\\\\?refer");
                PrintStream printStream2 = System.out;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("getVideoURL ");
                sb2.append(split.length);
                printStream2.println(sb2.toString());
                int i = 0;
                while (true) {
                    if (i >= split.length) {
                        break;
                    }
                    PrintStream printStream3 = System.out;
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("aaaaaaaaaaaaa=");
                    sb3.append(split[i]);
                    printStream3.println(sb3.toString());
                    if (split[i].contains("aweme")) {
                        str = split[i];
                        break;
                    }
                    i++;
                }
                PrintStream printStream4 = System.out;
                StringBuilder sb4 = new StringBuilder();
                sb4.append("list=");
                sb4.append(str);
                printStream4.println(sb4.toString());
                if (str == null) {
                    return null;
                }
                String[] split2 = str.split("\\\\?refer");
                PrintStream printStream5 = System.out;
                StringBuilder sb5 = new StringBuilder();
                sb5.append("dummy=");
                sb5.append(split2[0]);
                printStream5.println(sb5.toString());
                String[] split3 = split2[0].split("detail/");
                PrintStream printStream6 = System.out;
                StringBuilder sb6 = new StringBuilder();
                sb6.append("url=");
                sb6.append(Arrays.toString(split3[0].split("aweme")));
                printStream6.println(sb6.toString());
                PrintStream printStream7 = System.out;
                StringBuilder sb7 = new StringBuilder();
                sb7.append("u=");
                sb7.append(split3[1]);
                printStream7.println(sb7.toString());
                String replace = split3[1].replace("?", "");
                PrintStream printStream8 = System.out;
                StringBuilder sb8 = new StringBuilder();
                sb8.append("a=");
                sb8.append(replace);
                printStream8.println(sb8.toString());
                DashboardFragment dashboardFragment = DashboardFragment.this;
                StringBuilder sb9 = new StringBuilder();
                sb9.append("http://api2-21-h2.musical.ly/aweme/v1/aweme/detail/?aweme_id=");
                sb9.append(replace);
                sb9.append("&origin_type=web&retry_type=no_retry&iid=6693100971591812870&device_id=6613188380245214721&ac=wifi&channel=googleplay&aid=1233&app_name=musical_ly&version_code=110203&version_name=11.2.3&device_platform=android&ab_version=11.2.3&ssmix=a&device_type=LS-5021&device_brand=LYF&language=en&os_api=22&os_version=5.1.1&openudid=68fd205ac266b086&manifest_version_code=2019051203&resolution=1080*1920&dpi=480&update_version_code=2019051203&_rticket=1560166739657&build_number=11.2.3&region=US&uoo=0&timezone_name=Asia%2FCalcutta&timezone_offset=19800&pass-route=1&residence=IN&sys_region=IN&app_language=en&pass-region=1&is_my_cn=0&current_region=IN&ts=1560166825&as=a16574af394a3cf1be2288&cp=47accf5996edf610e1auOy&mas=01d8440f80e2b014380fff5e447dc34db31c1c4c4ca6468c66c6cc");
                dashboardFragment.newVideoURL = sb9.toString();
                PrintStream printStream9 = System.out;
                StringBuilder sb10 = new StringBuilder();
                sb10.append("new=");
                sb10.append(DashboardFragment.this.newVideoURL);
                printStream9.println(sb10.toString());
                DashboardFragment dashboardFragment2 = DashboardFragment.this;
                FragmentActivity activity = DashboardFragment.this.getActivity();
                activity.getClass();
                dashboardFragment2.queue1 = Volley.newRequestQueue(activity);
                StringRequest r5 = new StringRequest(1, DashboardFragment.this.newVideoURL, new C28011(), new C28022()) {
                    public String getBodyContentType() {
                        return "application/x-www-form-urlencoded; charset=utf-8";
                    }

                    public int getMethod() {
                        return 1;
                    }

                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap hashMap = new HashMap();
                        hashMap.put(Constants.USER_AGENT, "com.zhiliaoapp.musically/2019051203 (Linux; U; Android 6.1.1; en_IN; LS-5021; Build/LMY47V; Cronet/58.0.2991.0)");
                        return hashMap;
                    }
                };
                r5.setRetryPolicy(new DefaultRetryPolicy(60000, 1, 1.0f));
                DashboardFragment.this.queue1.add(r5);
                PrintStream printStream10 = System.out;
                StringBuilder sb11 = new StringBuilder();
                sb11.append("requestttttttt=");
                sb11.append(r5);
                printStream10.println(sb11.toString());
                PrintStream printStream11 = System.out;
                StringBuilder sb12 = new StringBuilder();
                sb12.append("queue=");
                sb12.append(DashboardFragment.this.queue1.toString());
                printStream11.println(sb12.toString());
                PrintStream printStream12 = System.out;
                StringBuilder sb13 = new StringBuilder();
                sb13.append("get=");
                sb13.append(r5);
                printStream12.println(sb13.toString());
                DashboardFragment.this.newVideoURL.replaceAll("\\\\", "").replace("u0026", "&").split("\"]");
                String str2 = split3[0];
                PrintStream printStream13 = System.out;
                StringBuilder sb14 = new StringBuilder();
                sb14.append(" final_result::");
                sb14.append(DashboardFragment.this.newVideoURL);
                printStream13.println(sb14.toString());
                return DashboardFragment.this.download_url;
            } catch (ArrayIndexOutOfBoundsException e) {
                PrintStream printStream14 = System.out;
                StringBuilder sb15 = new StringBuilder();
                sb15.append("Do not cross size of array sir.");
                sb15.append(e);
                printStream14.println(sb15.toString());
                return null;
            } catch (IOException e2) {
                e2.printStackTrace();
                PrintStream printStream15 = System.out;
                StringBuilder sb16 = new StringBuilder();
                sb16.append("IOException in Scraping Data::");
                sb16.append(e2.toString());
                printStream15.println(sb16.toString());
                return null;
            } catch (Exception e3) {
                PrintStream printStream16 = System.out;
                StringBuilder sb17 = new StringBuilder();
                sb17.append("Exception ");
                sb17.append(e3);
                printStream16.println(sb17.toString());
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String str) {
            super.onPostExecute(str);
            PrintStream printStream = System.out;
            StringBuilder sb = new StringBuilder();
            sb.append("FINAL URL ");
            sb.append(DashboardFragment.this.download_url);
            printStream.println(sb.toString());
            DashboardFragment.this.dismissDialog();
        }
    }

    public View onCreateView(@NonNull LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.rootView = layoutInflater.inflate(R.layout.fragment_main, viewGroup, false);
        this.context = getActivity();
        PRDownloader.initialize(this.context);
        intView(this.rootView);
        isAPrimeMember = this.context.getSharedPreferences("musically", 0).getBoolean("isapremiummember", false);
        PrintStream printStream = System.out;
        StringBuilder sb = new StringBuilder();
        sb.append("isAPrimeMember ");
        sb.append(isAPrimeMember);
        printStream.println(viewGroup.toString());

        String postUrl=((MainActivity)getActivity()).getPostUrl();

        if (postUrl !=null){
            searchText.setText(postUrl);
        }

        loadNativeAd();

        return this.rootView;
    }

    private void intView(View view) {

        searchText = (EditText) view.findViewById(R.id.post_url);
        Button button = (Button) view.findViewById(R.id.search_image);
        Button button2 = (Button) view.findViewById(R.id.open_tiktok);
        final LinearLayout llurl = (LinearLayout) view.findViewById(R.id.llurl);
        final Button btnPaste = (Button) view.findViewById(R.id.btnPaste);
        view.findViewById(R.id.search_image).setOnClickListener(this);
        view.findViewById(R.id.open_tiktok).setOnClickListener(this);
        btnPaste.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchText.getText().toString().length()!=0){
                    searchText.getText().clear();
                }else {
                    ClipboardManager  myClipboard = (ClipboardManager)context.getSystemService(CLIPBOARD_SERVICE);
                    ClipData abc = myClipboard.getPrimaryClip();

                    if (abc !=null){
                        ClipData.Item item = abc.getItemAt(0);
                        if (pasteDataFromURL(item.getText().toString())){
                            String text = item.getText().toString();
                            searchText.setText(text);
                        }
                    }

                }
            }
        });
        EditText editText = this.searchText;
        editText.setMaxWidth(editText.getWidth());
        this.searchText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (charSequence.length() > 0) {
                   // btnPaste.setVisibility(View.VISIBLE);
                    btnPaste.setText("Clear");
                    btnPaste.setBackgroundResource(R.drawable.red_default_button_style);
                    llurl.setBackgroundResource(R.drawable.red_edit_text_border);
                } else {
                   // btnPaste.setVisibility(View.GONE);
                    btnPaste.setText("Paste");
                    btnPaste.setBackgroundResource(R.drawable.default_button_style);
                    llurl.setBackgroundResource(R.drawable.edit_text_border);
                }
            }
        });
    }

    public void onClick(View view) {
        hideKeyboard(this.context);
        view.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.imageclick));
        if (CommonMethods.isNetworkAvailable(this.context)) {
            int id = view.getId();
            if (id == R.id.open_tiktok) {
                openTikTok();
            } else if (id == R.id.search_image) {
                scrapDataFromURL();
            }
        } else {
            simpleSnackBar(view, "No internet connection !!");
        }
    }

    private void openTikTok() {
        Intent launchIntentForPackage = this.context.getPackageManager().getLaunchIntentForPackage(getString(R.string.str_tiktok_package_name));
        if (launchIntentForPackage != null) {
            startActivity(launchIntentForPackage);
        } else {
            Toast.makeText(getContext(), getString(R.string.str_tiktok_not_installed), Toast.LENGTH_SHORT).show();
        }
    }

    private void scrapDataFromURL() {
        if (this.searchText.getText().toString().isEmpty()) {
            simpleSnackBar(this.searchText, "Please enter a URL to download...");
            return;
        }
        String obj = this.searchText.getText().toString();
        if (obj.toLowerCase().contains("tiktok") || obj.toLowerCase().contains("musical.ly")) {
            LinkSpan linkSpan = (LinkSpan) LinkExtractor.builder().linkTypes(EnumSet.of(LinkType.URL, LinkType.WWW, LinkType.EMAIL)).build().extractLinks(this.searchText.getText().toString()).iterator().next();
            linkSpan.getType();
            linkSpan.getBeginIndex();
            linkSpan.getEndIndex();
            String substring = obj.substring(linkSpan.getBeginIndex(), linkSpan.getEndIndex());
            this.downloadFileFromURLTask = new DownloadFileFromURL(substring);
            this.downloadFileFromURLTask.execute(new String[0]);
            PrintStream printStream = System.out;
            StringBuilder sb = new StringBuilder();
            sb.append("Final urlllllllllllllllll::::");
            sb.append(substring);
            printStream.println(sb.toString());
            return;
        }
        simpleSnackBar(this.searchText, "Please enter a valid URL...");
    }

    private boolean pasteDataFromURL(String obj) {
        if (obj.toLowerCase().contains("tiktok") || obj.toLowerCase().contains("musical.ly")) {

            return true;
        }
        return  false;
    }

    private void simpleSnackBar(View view, String str) {
        Snackbar make = Snackbar.make(view.findViewById(view.getId()), (CharSequence) str, Snackbar.LENGTH_LONG);
        ((TextView) make.getView().findViewById(R.id.snackbar_text)).setTextColor(Color.YELLOW);
        make.show();
    }

    public void onDestroy() {
        DownloadFileFromURL downloadFileFromURL = this.downloadFileFromURLTask;
        if (downloadFileFromURL != null && downloadFileFromURL.getStatus() == Status.RUNNING) {
            this.downloadFileFromURLTask.cancel(true);
        }
        dismissDialog();
        dismissDownloadingDialog();
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void showProgressDialog() {
        if (this.progressDialog == null) {
            this.progressDialog = new ProgressDialog(this.context);
            this.progressDialog.setProgressStyle(16842873);
            this.progressDialog.setMessage("Downloading Video....");
            this.progressDialog.setCancelable(false);
        }
        this.progressDialog.show();
    }

    /* access modifiers changed from: private */
    public void dismissDialog() {
        ProgressDialog progressDialog2 = this.progressDialog;
        if (progressDialog2 != null && progressDialog2.isShowing()) {
            this.progressDialog.dismiss();
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View currentFocus = activity.getCurrentFocus();
        if (currentFocus == null) {
            currentFocus = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
    }

    private void showDownloadingDialog() {
        if (downloadingDialog == null) {
            downloadingDialog = new Dialog(context);
            downloadingDialog.requestWindowFeature(1);
            downloadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            downloadingDialog.setContentView(R.layout.download_via_url);
            downloadingDialog.setCancelable(false);
            progressText = (TextView) downloadingDialog.findViewById(R.id.progress);
            circularProgressBar =downloadingDialog.findViewById(R.id.circularProgressBar);
        }
        downloadingDialog.show();
    }

    /* access modifiers changed from: private */
    public void dismissDownloadingDialog() {
        Dialog dialog = this.downloadingDialog;
        if (dialog != null && dialog.isShowing()) {
            this.downloadingDialog.dismiss();
        }
    }

    public void downloadMedia(String str) {
        showDownloadingDialog();
        File file = new File(CommonMethods.pathName1);
        if (!file.exists()) {
            file.mkdirs();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(System.currentTimeMillis());
        sb.append(".mp4");
        final String sb2 = sb.toString();
        this.downloadId = PRDownloader.download(str, CommonMethods.pathName1, sb2).build().setOnStartOrResumeListener(new Players()).setOnPauseListener(new VidPlayer()).setOnCancelListener(new progressClass()).setOnProgressListener(new ProgressListners()).start(new OnDownloadListener() {
            public void onDownloadComplete() {
                DashboardFragment.this.downloadId = 0;
                DashboardFragment.this.dismissDownloadingDialog();
                DownloadedFragment.fetchingMediaFromDirectory();
                if (VERSION.SDK_INT >= 19) {
                    Intent intent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
                    StringBuilder sb = new StringBuilder();
                    sb.append("file://");
                    sb.append(CommonMethods.pathName1);
                    intent.setData(Uri.parse(sb.toString()));
                    DashboardFragment.this.getActivity().sendBroadcast(intent);
                    return;
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append("file://");
                sb2.append(CommonMethods.pathName1);
                DashboardFragment.this.getActivity().sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse(sb2.toString())));
            }

            public void onError(Error error) {
                DashboardFragment.this.dismissDownloadingDialog();
                Toast.makeText(DashboardFragment.this.context, "Download Failed", Toast.LENGTH_LONG).show();
                StringBuilder sb = new StringBuilder();
                sb.append(CommonMethods.pathName1);
                sb.append(sb2);
                File file = new File(sb.toString());
                if (file.exists()) {
                    file.delete();
                }
            }
        });
        com.downloader.Status status = PRDownloader.getStatus(this.downloadId);
        PrintStream printStream = System.out;
        StringBuilder sb3 = new StringBuilder();
        sb3.append("Status of download is ::");
        sb3.append(status);
        printStream.println(sb3.toString());
    }


    private void loadNativeAd() {
        // Instantiate a NativeAd object.
        // NOTE: the placement ID will eventually identify this as your App, you can ignore it for
        // now, while you are testing and replace it later when you have signed up.
        // While you are using this temporary code you will only get test ads and if you release
        // your code like this to the Google Play your users will not receive ads (you will get a no fill error).
        nativeAd = new NativeAd(context, getResources().getString(R.string.native_placement_id));

        nativeAd.setAdListener(new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {

            }

            @Override
            public void onError(Ad ad, AdError adError) {
                Toast.makeText(context, "Failed to load native add"+adError.toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Race condition, load() called again before last ad was displayed
                if (nativeAd == null || nativeAd != ad) {
                    return;
                }
                // Inflate Native Ad into Container
                inflateAd(nativeAd);
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }

        });

        // Request an ad
        nativeAd.loadAd();
    }

    private void inflateAd(NativeAd nativeAd) {

        nativeAd.unregisterView();

        // Add the Ad view into the ad container.
        nativeAdLayout = rootView.findViewById(R.id.native_ad_container);
        LayoutInflater inflater = LayoutInflater.from(context);
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
        adView = (LinearLayout) inflater.inflate(R.layout.native_ad_layout, nativeAdLayout, false);
        nativeAdLayout.addView(adView);

        // Add the AdOptionsView
        LinearLayout adChoicesContainer = rootView.findViewById(R.id.ad_choices_container);
        AdOptionsView adOptionsView = new AdOptionsView(context, nativeAd, nativeAdLayout);
        adChoicesContainer.removeAllViews();
        adChoicesContainer.addView(adOptionsView, 0);

        // Create native UI using the ad metadata.
        AdIconView nativeAdIcon = adView.findViewById(R.id.native_ad_icon);
        TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title);
        MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);
        TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);
        TextView nativeAdBody = adView.findViewById(R.id.native_ad_body);
        TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);
        Button nativeAdCallToAction = adView.findViewById(R.id.native_ad_call_to_action);

        // Set the Text.
        nativeAdTitle.setText(nativeAd.getAdvertiserName());
        nativeAdBody.setText(nativeAd.getAdBodyText());
        nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
        nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
        nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

        // Create a list of clickable views
        List<View> clickableViews = new ArrayList<>();
        clickableViews.add(nativeAdTitle);
        clickableViews.add(nativeAdCallToAction);

        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
                adView,
                nativeAdMedia,
                nativeAdIcon,
                clickableViews);
    }
}
