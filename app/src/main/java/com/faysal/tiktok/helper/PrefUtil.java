package com.faysal.tiktok.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Lincoln on 05/05/16.
 */
public class PrefUtil {
    private static int PRIVATE_MODE=0;
    static SharedPreferences pref;
    static SharedPreferences.Editor editor;
    public static final String PREF_KEY_APP_AUTO_START="auto_start";
    // shared pref mode

    // Shared preferences file name
    private static final String PREF_NAME = "tiktoksAutoStart";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";



    public static void writeBoolean(Context context,String codename,boolean isFirstTime) {
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.putBoolean(codename, isFirstTime);
        editor.commit();
    }

    public boolean getData(Context context,String codename) {
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        return pref.getBoolean(codename, false);
    }

}