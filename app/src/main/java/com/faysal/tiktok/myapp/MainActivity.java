package com.faysal.tiktok.myapp;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.facebook.ads.AudienceNetworkAds;
import com.faysal.tiktok.ActivityGDPR;
import com.faysal.tiktok.Adapters.ViewPagerAdapter;
import com.faysal.tiktok.ClipboardService.AutoDirectService;
import com.faysal.tiktok.ClipboardService.NotiService;
import com.faysal.tiktok.Fragments.FragmentHowToUse;
import com.faysal.tiktok.R;
import com.faysal.tiktok.Fragments.DashboardFragment;
import com.faysal.tiktok.Fragments.DownloadedFragment;
import com.faysal.tiktok.SettingsActivity;
import com.faysal.tiktok.Utils.AutoStartHelper;
import com.faysal.tiktok.Utils.CommonMethods;
import com.faysal.tiktok.Utils.Constants;
import com.faysal.tiktok.helper.PrefManager;
import com.faysal.tiktok.helper.SharedHelper;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdRequest.Builder;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.tabs.TabLayout;

import java.io.PrintStream;

public class MainActivity extends AppCompatActivity {
    public static ViewPager pager;
    public static TabLayout tabLayout;
    LinearLayout banner_adview;
    AdView banneradView;
    /* access modifiers changed from: private */
    public Dialog dialog;
    private Editor editor;
    /* access modifiers changed from: private */
    public Boolean exit = Boolean.valueOf(false);
    /* access modifiers changed from: private */
    public InterstitialAd mInterstitialAd;
    int play_video_count = 0;
    PrefManager manager;

    String postUrl=null;

    class C11913 implements OnClickListener {
        C11913() {
        }

        public void onClick(View view) {
            view.startAnimation(AnimationUtils.loadAnimation(MainActivity.this.getApplicationContext(), R.anim.imageclick));
            MainActivity.this.dialog.dismiss();
        }
    }

    class Runnales implements Runnable {
        Runnales() {
        }

        public void run() {
            MainActivity.this.exit = Boolean.valueOf(false);
        }
    }

    class C18092 implements ViewPager.OnPageChangeListener {
        public void onPageScrollStateChanged(int i) {
        }

        public void onPageScrolled(int i, float f, int i2) {
        }

        public void onPageSelected(int i) {
        }

        C18092() {
        }
    }

    class InstructionsPopUp implements DialogInterface.OnClickListener {
        InstructionsPopUp() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    class interstitialAdListener extends AdListener {
        final AdRequest adReq;

        interstitialAdListener(AdRequest adRequest) {
            this.adReq = adRequest;
        }

        public void onAdClosed() {
            MainActivity.this.mInterstitialAd.loadAd(this.adReq);
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint("WrongViewCast")
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_main);
        AudienceNetworkAds.initialize(this);
        manager=new PrefManager(this);
        MobileAds.initialize(this);

        if (SharedHelper.getKey(this,"first_impression").equals("false")){
            SharedHelper.putKey(this,"first_impression","true");
            startActivity(new Intent(this, ActivityGDPR.class));
            finish();
            return;
        }


        Intent intent=getIntent();

        if (intent !=null){
            String url=intent.getStringExtra("url");
            if (url !=null){
                postUrl=url;
            }
        }

       // AutoStartHelper.getInstance().getAutoStartPermission(this);


        if (SharedHelper.getKey(this,"first_launch").equals("false")){
           startService(new Intent(this, AutoDirectService.class));
           startService(new Intent(this, NotiService.class));
            SharedHelper.putKey(this,"notification","true");
            SharedHelper.putKey(this,"auto_direct","true");
            SharedHelper.putKey(this,"first_launch","true");
        }

        if (SharedHelper.getKey(this,"auto_direct").equals("true")){
            if (!isMyServiceRunning(AutoDirectService.class))
                 startService(new Intent(this, AutoDirectService.class));
        }else {
            if (isMyServiceRunning(AutoDirectService.class))
                  stopService(new Intent(this, AutoDirectService.class));
        }


        if (SharedHelper.getKey(this,"notification").equals("true")){
            if (!isMyServiceRunning(AutoDirectService.class))
                 startService(new Intent(this, NotiService.class));
        }else {
            if (isMyServiceRunning(AutoDirectService.class))
              stopService(new Intent(this, NotiService.class));
        }

        this.editor = getSharedPreferences("musically", 0).edit();
        if (ContextCompat.checkSelfPermission(this, "android.permission.READ_EXTERNAL_STORAGE") != 0) {
            askPermission_gallery();
        } else {
            CommonMethods.createDirectory();
            System.out.println("Code reader cannot come here");
        }


        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle("TikTok Downloader");
        pager = (ViewPager) findViewById(R.id.mainPager);
        tabLayout = (TabLayout) findViewById(R.id.mainTabs);
        setupViewPager(pager);
        tabLayout.setupWithViewPager(pager);
        pager.setOnPageChangeListener(new C18092());
        banner_adview = (LinearLayout) findViewById(R.id.admob_layout2);
        banneradView = (AdView) findViewById(R.id.adView2);
        AdRequest build = new Builder().build();
        banneradView.loadAd(build);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.interstitial_ads));
        mInterstitialAd.loadAd(new Builder().build());
        mInterstitialAd.setAdListener(new interstitialAdListener(build));
        banner_adview.setVisibility(View.VISIBLE);
       // new AppRater(this).show();



    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }



     public String getPostUrl(){
        if (postUrl !=null){
           // pager.setCurrentItem(0);
            return postUrl;
        }
        return null;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));


                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }


 /*   public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_follow *//*2131361811*//*:
                followUs();
                return true;
            case R.id.action_moreapps *//*2131361818*//*:
                moreApps();
                return true;
            case R.id.action_sharethisapp *//*2131361819*//*:
                shareApp();
                return true;
            case R.id.action_watch_video *//*2131361821*//*:
                watchVideo();
                return true;
            case R.id.instructions_btn *//*2131361905*//*:
                instructions();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }*/

    public boolean onMenuOpened(int i, Menu menu) {
        if (this.mInterstitialAd.isLoaded()) {
            this.mInterstitialAd.show();
        } else {
            PrintStream printStream = System.out;
            StringBuilder sb = new StringBuilder();
            sb.append("The interstitial wasn't loaded yet.");
            printStream.println(sb.toString());
        }
        return super.onMenuOpened(i, menu);
    }

    public void instructions() {
        new AlertDialog.Builder(this).setTitle("TikTok Downloader Instructions").setMessage(R.string.instructions_txt).setPositiveButton("Ok", new InstructionsPopUp()).show();
    }

    public void shareApp() {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("text/plain");
        intent.putExtra("android.intent.extra.TEXT", "Check out this awesome app to remove watermark from TikTok videos: https://play.google.com/store/apps/details?id=com.faysal.tiktok");
        startActivity(Intent.createChooser(intent, "Share link via"));
    }

    public void moreApps() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("https://play.google.com/store/apps/developer?id=Fun Artica"));
        startActivity(intent);
    }

    public void watchVideo() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("https://www.youtube.com/watch?v=Wc7fNNgHdlc"));
        startActivity(intent);
    }

    public void followUs() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("https://www.instagram.com/tiktokvideodownloader/"));
        startActivity(intent);
    }

    /* access modifiers changed from: 0000 */
    public void askPermission_gallery() {
        if (VERSION.SDK_INT < 23) {
            CommonMethods.createDirectory();
        } else if (ContextCompat.checkSelfPermission(this, "android.permission.READ_EXTERNAL_STORAGE") == 0 || ContextCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") == 0) {
            CommonMethods.createDirectory();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"}, 2);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        ViewPagerAdapter viewPagerAdapter2 = viewPagerAdapter;
        viewPagerAdapter2.addFrag(new DashboardFragment(), "Download");
        viewPagerAdapter2.addFrag(new DownloadedFragment(), "Library");
        viewPagerAdapter2.addFrag(new FragmentHowToUse(), "How to use");
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(0);


    }

    public void setPagerItem(int id){
        pager.setCurrentItem(id);
    }

    public void onBackPressed() {
        try {
            if (this.exit.booleanValue()) {
                finish();
                return;
            }
            Toast.makeText(this, "Tap Again to Exit.", Toast.LENGTH_SHORT).show();
            this.exit = Boolean.valueOf(true);
            new Handler().postDelayed(new Runnales(), 3000);
        } catch (Exception e) {
            PrintStream printStream = System.out;
            StringBuilder sb = new StringBuilder();
            sb.append("hahahah BUG::;");
            sb.append(e.getMessage());
            printStream.println(sb.toString());
        }
    }

    public void onRequestPermissionsResult(int i, @NonNull String[] strArr, @NonNull int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        CommonMethods.createDirectory();
    }
}
