package com.faysal.tiktok;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.faysal.tiktok.myapp.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityHTU extends AppCompatActivity {

    @BindView(R.id.toolbar)
         LinearLayout toolbar;

    @BindView(R.id.btnokunderstand)
    Button btnokunderstand;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_howtouse);
        ButterKnife.bind(this);

        toolbar.setVisibility(View.VISIBLE);
        btnokunderstand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }
        });



    }
}
