package com.faysal.tiktok;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.faysal.tiktok.ClipboardService.AutoDirectService;
import com.faysal.tiktok.ClipboardService.NotiService;
import com.faysal.tiktok.Utils.Constants;
import com.faysal.tiktok.helper.PrefManager;
import com.faysal.tiktok.helper.SharedHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout show_in_notification_bar;
    LinearLayout auto_detect;
    LinearLayout share_layout;
    LinearLayout rate_downloader_layout;
    LinearLayout more_apps_layout;
    LinearLayout personalization_layout;
    LinearLayout privacy_policy_layout;
    TextView versone_name;

    CheckBox notificaions_checkbox;
    CheckBox auto_detect_checkbox;

    Context context;
    
    public static final String TAG=SettingsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        context=this;

        initView();

    }

    private void initView(){

        notificaions_checkbox=findViewById(R.id.notificaions_checkbox);
        auto_detect_checkbox=findViewById(R.id.auto_detect_checkbox);
        share_layout=findViewById(R.id.share_layout);
        rate_downloader_layout=findViewById(R.id.rate_downloader_layout);
        more_apps_layout=findViewById(R.id.more_apps_layout);
        personalization_layout=findViewById(R.id.personalization_layout);
        privacy_policy_layout=findViewById(R.id.privacy_policy_layout);
        versone_name=findViewById(R.id.versone_name);

        show_in_notification_bar=findViewById(R.id.show_in_notification_bar);
        auto_detect=findViewById(R.id.auto_detect);



        versone_name.setText("Version v"+BuildConfig.VERSION_NAME);

        show_in_notification_bar.setOnClickListener(this);
        auto_detect.setOnClickListener(this);
        share_layout.setOnClickListener(this);
        rate_downloader_layout.setOnClickListener(this);
        more_apps_layout.setOnClickListener(this);
        privacy_policy_layout.setOnClickListener(this);
        personalization_layout.setOnClickListener(this);


        if (SharedHelper.getKey(context,"notification").equals("true")){
            notificaions_checkbox.setChecked(true);
        }else {
            notificaions_checkbox.setChecked(false);
        }

        if (SharedHelper.getKey(context,"auto_direct").equals("true")){
            auto_detect_checkbox.setChecked(true);
        }else {
            auto_detect_checkbox.setChecked(false);
        }
    }

    @Override
    public void onClick(View v) {
        int id=v.getId();

        switch (id){
            case R.id.auto_detect :
                if (auto_detect_checkbox.isChecked()){
                    auto_detect_checkbox.setChecked(false);
                    SharedHelper.putKey(context,"auto_direct","false");
                    if (isMyServiceRunning(AutoDirectService.class))
                        stopService(new Intent(this, AutoDirectService.class));

                }else {
                    auto_detect_checkbox.setChecked(true);
                    SharedHelper.putKey(context,"auto_direct","true");
                    if (!isMyServiceRunning(AutoDirectService.class))
                         startService(new Intent(this, AutoDirectService.class));

                }
                break;

            case R.id.show_in_notification_bar :
                if (notificaions_checkbox.isChecked()){
                    notificaions_checkbox.setChecked(false);
                    SharedHelper.putKey(context,"notification","false");
                    if (isMyServiceRunning(NotiService.class))
                        stopService(new Intent(this, NotiService.class));
                }else {
                    notificaions_checkbox.setChecked(true);
                    SharedHelper.putKey(context,"notification","true");
                    if (!isMyServiceRunning(NotiService.class))
                        startService(new Intent(this, NotiService.class));
                }
               break;
            case R.id.share_layout :
               shareApp();

                break;
            case R.id.more_apps_layout :
               moreApps();

                break;
            case R.id.rate_downloader_layout :
               rateApp();

                break;
            case R.id.personalization_layout :
               startActivity(new Intent(this,ActivityGDPR.class));

                break;
            case R.id.privacy_policy_layout :
               startActivity(new Intent(this,PrivacyPolicy.class));

                break;
        }
    }


    public void shareApp() {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("text/plain");
        intent.putExtra("android.intent.extra.TEXT", "Check out this awesome app to remove watermark from TikTok videos: https://play.google.com/store/apps/details?id="+this.getPackageName());
        startActivity(Intent.createChooser(intent, "Share link via"));
    }

    public void moreApps() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("https://play.google.com/store/apps/developer?id=Faysal Ahmed Shakil"));
        startActivity(intent);
    }

    public void rateApp() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.faysal.tiktok"+this.getPackageName()));
        startActivity(intent);
    }


    public void followUs() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("https://www.instagram.com/tiktokvideodownloader/"));
        startActivity(intent);
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}