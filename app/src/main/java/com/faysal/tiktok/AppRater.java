package com.faysal.tiktok;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build.VERSION;

public class AppRater {
    private static final int DEFAULT_DAYS_BEFORE_PROMPT = 0;
    private static final int DEFAULT_LAUNCHES_BEFORE_PROMPT = 2;
    private static final String DEFAULT_PREFERENCE_DONT_SHOW = "flag_dont_show";
    private static final String DEFAULT_PREFERENCE_FIRST_LAUNCH = "first_launch_time";
    private static final String DEFAULT_PREFERENCE_LAUNCH_COUNT = "launch_count";
    private static final String DEFAULT_PREF_GROUP = "app_rater";
    private static final String DEFAULT_TARGET_URI = "https://play.google.com/store/apps/details?id=com.faysal.tiktok";
    private static final String DEFAULT_TEXT_EXPLANATION = "If you love our app please take a moment to rate it.";
    private static final String DEFAULT_TEXT_FEEDBACK = "Send Feedback";
    private static final String DEFAULT_TEXT_NOW = "Rate Now";
    private static final String DEFAULT_TEXT_TITLE = "Rate us 5 Stars!";
    private Context mContext;
    private int mDaysBeforePrompt;
    private int mLaunchesBeforePrompt;
    private String mPackageName;
    private String mPrefGroup;
    private String mPreference_dontShow;
    private String mPreference_firstLaunch;
    private String mPreference_launchCount;
    private Intent mTargetIntent;
    private String mTargetUri;
    private String mText_buttonFeedback;
    private String mText_buttonNow;
    private String mText_explanation;
    private String mText_title;

    public AppRater(Context context) {
        this(context, context.getPackageName());
    }

    public AppRater(Context context, String str) {
        if (context != null) {
            this.mContext = context;
            this.mPackageName = str;
            this.mDaysBeforePrompt = 0;
            this.mLaunchesBeforePrompt = 2;
            this.mTargetUri = DEFAULT_TARGET_URI;
            this.mText_title = DEFAULT_TEXT_TITLE;
            this.mText_explanation = DEFAULT_TEXT_EXPLANATION;
            this.mText_buttonNow = DEFAULT_TEXT_NOW;
            this.mText_buttonFeedback = DEFAULT_TEXT_FEEDBACK;
            this.mPrefGroup = DEFAULT_PREF_GROUP;
            this.mPreference_dontShow = DEFAULT_PREFERENCE_DONT_SHOW;
            this.mPreference_launchCount = DEFAULT_PREFERENCE_LAUNCH_COUNT;
            this.mPreference_firstLaunch = DEFAULT_PREFERENCE_FIRST_LAUNCH;
            return;
        }
        throw new RuntimeException("context may not be null");
    }

    public void setDaysBeforePrompt(int i) {
        this.mDaysBeforePrompt = i;
    }

    public void setLaunchesBeforePrompt(int i) {
        this.mLaunchesBeforePrompt = i;
    }

    public void setTargetUri(String str) {
        this.mTargetUri = str;
    }

    public void setPhrases(String str, String str2, String str3, String str4) {
        this.mText_title = str;
        this.mText_explanation = str2;
        this.mText_buttonNow = str3;
        this.mText_buttonFeedback = str4;
    }

    public void setPhrases(int i, int i2, int i3, int i4) {
        try {
            this.mText_title = this.mContext.getString(i);
        } catch (Exception unused) {
            this.mText_title = DEFAULT_TEXT_TITLE;
        }
        try {
            this.mText_explanation = this.mContext.getString(i2);
        } catch (Exception unused2) {
            this.mText_explanation = DEFAULT_TEXT_EXPLANATION;
        }
        try {
            this.mText_buttonNow = this.mContext.getString(i3);
        } catch (Exception unused3) {
            this.mText_buttonNow = DEFAULT_TEXT_NOW;
        }
        try {
            this.mText_buttonFeedback = this.mContext.getString(i4);
        } catch (Exception unused4) {
            this.mText_buttonFeedback = DEFAULT_TEXT_FEEDBACK;
        }
    }

    public void setPreferenceKeys(String str, String str2, String str3, String str4) {
        this.mPrefGroup = str;
        this.mPreference_dontShow = str2;
        this.mPreference_launchCount = str3;
        this.mPreference_firstLaunch = str4;
    }

    private void createTargetIntent() {
        this.mTargetIntent = new Intent("android.intent.action.VIEW", Uri.parse(String.format(this.mTargetUri, new Object[]{this.mPackageName})));
    }

    @SuppressLint({"CommitPrefEdits"})
    public AlertDialog show() {
        createTargetIntent();
        if (this.mContext.getPackageManager().queryIntentActivities(this.mTargetIntent, 0).size() <= 0) {
            return null;
        }
        SharedPreferences sharedPreferences = this.mContext.getSharedPreferences(this.mPrefGroup, 0);
        Editor edit = sharedPreferences.edit();
        if (sharedPreferences.getBoolean(this.mPreference_dontShow, false)) {
            return null;
        }
        long j = sharedPreferences.getLong(this.mPreference_launchCount, 0) + 1;
        edit.putLong(this.mPreference_launchCount, j);
        long j2 = sharedPreferences.getLong(this.mPreference_firstLaunch, 0);
        if (j2 == 0) {
            j2 = System.currentTimeMillis();
            edit.putLong(this.mPreference_firstLaunch, j2);
        }
        savePreferences(edit);
        if (j < ((long) this.mLaunchesBeforePrompt) || System.currentTimeMillis() < (((long) this.mDaysBeforePrompt) * 86400000) + j2) {
            return null;
        }
        try {
            return showDialog(this.mContext, edit, j2);
        } catch (Exception unused) {
            return null;
        }
    }

    @Deprecated
    public AlertDialog demo() {
        createTargetIntent();
        Context context = this.mContext;
        return showDialog(context, context.getSharedPreferences(this.mPrefGroup, 0).edit(), System.currentTimeMillis());
    }

    @SuppressLint({"NewApi"})
    private static void savePreferences(Editor editor) {
        if (editor == null) {
            return;
        }
        if (VERSION.SDK_INT < 9) {
            editor.commit();
        } else {
            editor.apply();
        }
    }

    private static void closeDialog(DialogInterface dialogInterface) {
        if (dialogInterface != null) {
            dialogInterface.dismiss();
        }
    }

    private void setDontShow(Editor editor) {
        if (editor != null) {
            editor.putBoolean(this.mPreference_dontShow, true);
            savePreferences(editor);
        }
    }

    private void setFirstLaunchTime(Editor editor, long j) {
        if (editor != null) {
            editor.putLong(this.mPreference_firstLaunch, j);
            savePreferences(editor);
        }
    }

    /* access modifiers changed from: private */
    public void buttonNowClick(Editor editor, DialogInterface dialogInterface, Context context) {
        setDontShow(editor);
        closeDialog(dialogInterface);
        context.startActivity(this.mTargetIntent);
    }

    /* access modifiers changed from: private */
    public void buttonLaterClick(Editor editor, DialogInterface dialogInterface, Context context) {
        setDontShow(editor);
        closeDialog(dialogInterface);
        Intent intent = new Intent("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.EMAIL", new String[]{"funarticans@gmail.com"});
        intent.putExtra("android.intent.extra.SUBJECT", "TikTok Downloader Feedback");
        intent.setType("text/plain");
        ResolveInfo resolveInfo = null;
        for (ResolveInfo resolveInfo2 : context.getPackageManager().queryIntentActivities(intent, 0)) {
            if (resolveInfo2.activityInfo.packageName.endsWith(".gm") || resolveInfo2.activityInfo.name.toLowerCase().contains("gmail")) {
                resolveInfo = resolveInfo2;
            }
        }
        if (resolveInfo != null) {
            intent.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
        }
        context.startActivity(intent);
    }

    private AlertDialog showDialog(final Context context, final Editor editor, long j) {
        Builder builder = new Builder(context);
        builder.setTitle(this.mText_title);
        builder.setMessage(this.mText_explanation);
        builder.setNeutralButton(this.mText_buttonFeedback, new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                AppRater.this.buttonLaterClick(editor, dialogInterface, context);
            }
        });
        builder.setPositiveButton(this.mText_buttonNow, new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                AppRater.this.buttonNowClick(editor, dialogInterface, context);
            }
        });
        return builder.show();
    }
}
