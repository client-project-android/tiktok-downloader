package com.faysal.tiktok.Utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.Environment;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;

public class CommonMethods {
    public static Boolean is_a_premium_member;
    public static final String pathName1;
    public static ArrayList<Integer> selectedPosition = new ArrayList<>();

    static {
        StringBuilder sb = new StringBuilder();
        sb.append(Environment.getExternalStorageDirectory());
        sb.append("/FS TikTok/");
        pathName1 = sb.toString();
    }

    public static boolean isExternalStorageWritable() {
        return "mounted".equals(Environment.getExternalStorageState());
    }

    public static void createDirectory() {
        System.out.println("Step 1::");
        if (isExternalStorageWritable()) {
            System.out.println("Step 1::");
            File file = new File(pathName1);
            System.out.println("Step 1::");
            if (file.exists()) {
                PrintStream printStream = System.out;
                StringBuilder sb = new StringBuilder();
                sb.append(":::Directory already created");
                sb.append(file.getAbsolutePath());
                printStream.println(sb.toString());
                return;
            }
            file.mkdir();
            PrintStream printStream2 = System.out;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(":::Directory created successfully");
            sb2.append(file.getAbsolutePath());
            printStream2.println(sb2.toString());
            return;
        }
        System.out.println("Not enough space");
    }

    public static boolean isNetworkAvailable(Activity activity) {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo[] allNetworkInfo = connectivityManager.getAllNetworkInfo();
        if (allNetworkInfo != null) {
            for (NetworkInfo state : allNetworkInfo) {
                if (state.getState() == State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void shareMediaThroughIntent(String str, String str2, int i, Activity activity) {
        try {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType(str);
            intent.putExtra("android.intent.extra.TEXT", "Share Video Via Tiktok Downloader");
            intent.addFlags(1);
            intent.putExtra("android.intent.extra.STREAM", FileProvider.getUriForFile(activity, "com.faysal.tiktok.provider", new File(str2)));
            if (i == 1) {
                try {
                    activity.startActivity(Intent.createChooser(intent, "Share Video"));
                } catch (ActivityNotFoundException unused) {
                    Toast.makeText(activity, "No app can perform this action", 0).show();
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
