package com.faysal.tiktok.ClipboardService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Faysal Ahmed Shakil on 12/10/2017.
 */

public class ClipReciver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())){
            context.startService(new Intent(context, AutoDirectService.class));
            context.startService(new Intent(context, NotiService.class));
        }

    }
}
