package com.faysal.tiktok.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.faysal.tiktok.Fragments.DashboardFragment;
import com.faysal.tiktok.R;
import com.faysal.tiktok.Utils.CommonMethods;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdRequest.Builder;
import com.google.android.gms.ads.InterstitialAd;
import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;

import com.google.android.exoplayer2.util.MimeTypes;
import com.rahman.dialog.Activity.SmartDialog;
import com.rahman.dialog.ListenerCallBack.SmartDialogClickListener;
import com.rahman.dialog.Utilities.SmartDialogBuilder;

public class MediaAdapter extends BaseAdapter {
    public static Activity context;
    public static String filename;
    public static ArrayList<String> imgid;
    /* access modifiers changed from: private */
    public InterstitialAd mInterstitialAd;
    /* access modifiers changed from: private */
    public int play_video_count = 0;

    class ViewHolder {
        ImageView Imaage;
        ImageView image_video;
        ImageView share_video;
        RelativeLayout share_video_layout;
        RelativeLayout delete_check_layout;

        ViewHolder() {
        }
    }

    class interstitialAdListener extends AdListener {
        final AdRequest adReq;

        interstitialAdListener(AdRequest adRequest) {
            this.adReq = adRequest;
        }

        public void onAdClosed() {
            MediaAdapter.this.mInterstitialAd.loadAd(this.adReq);
        }
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public MediaAdapter(Activity activity, ArrayList<String> arrayList) {
        context = activity;
        imgid = arrayList;
        this.mInterstitialAd = new InterstitialAd(context);
        this.mInterstitialAd.setAdUnitId("ca-app-pub-2558135703676173/3282116490");
        this.mInterstitialAd.loadAd(new Builder().build());
    }

    public int getCount() {
        return imgid.size();
    }

    public Object getItem(int i) {
        return imgid.get(i);
    }

    /* access modifiers changed from: private */
    public void playVideo(String str) {
        filename = str;
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PrintStream printStream = System.out;
            StringBuilder sb = new StringBuilder();
            sb.append("videopath");
            sb.append(str);
            printStream.println(sb.toString());
            intent.setDataAndType(Uri.parse(str), MimeTypes.VIDEO_MP4);
            context.startActivity(Intent.createChooser(intent, "Play with"));
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Please install any video player to play the video !", Toast.LENGTH_SHORT).show();
        }
    }

    public View getView(final int i, View view, ViewGroup viewGroup) {
        View view2;
        ViewHolder viewHolder;
        this.mInterstitialAd.setAdListener(new interstitialAdListener(new Builder().build()));
        if (view == null) {
            viewHolder = new ViewHolder();
            view2 = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.media_grid_list_item, viewGroup, false);
            viewHolder.Imaage = (ImageView) view2.findViewById(R.id.Imaage);
            viewHolder.image_video = (ImageView) view2.findViewById(R.id.image_video);
            viewHolder.share_video = (ImageView) view2.findViewById(R.id.share_video);
            viewHolder.share_video_layout = (RelativeLayout) view2.findViewById(R.id.share_layout);
            viewHolder.delete_check_layout = (RelativeLayout) view2.findViewById(R.id.delete_check_layout);
            view2.setTag(viewHolder);
        } else {
            view2 = view;
            viewHolder = (ViewHolder) view.getTag();
        }
        if (((String) imgid.get(i)).contains("mp4")) {
            viewHolder.image_video.setVisibility(View.VISIBLE);
            Glide.with(context).load(Uri.fromFile(new File((String) imgid.get(i)))).into(viewHolder.Imaage);
        } else {
            viewHolder.image_video.setVisibility(View.GONE);
            Glide.with(context).load(Uri.fromFile(new File((String) imgid.get(i)))).into(viewHolder.Imaage);
        }

        viewHolder.image_video.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                MediaAdapter.this.play_video_count = MediaAdapter.this.play_video_count + 1;
                PrintStream printStream = System.out;
                StringBuilder sb = new StringBuilder();
                sb.append("Ad ################");
                sb.append(MediaAdapter.this.play_video_count);
                printStream.print(sb.toString());
                if (MediaAdapter.this.mInterstitialAd.isLoaded() && MediaAdapter.this.play_video_count == 1) {
                    MediaAdapter.this.mInterstitialAd.show();
                }
                if (((String) MediaAdapter.imgid.get(i)).contains("mp4")) {
                    MediaAdapter.this.playVideo((String) MediaAdapter.imgid.get(i));
                }
            }
        });


        viewHolder.delete_check_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                String path=imgid.get(i);

                Log.d("ISDSDFSDF", "onClick: "+path);

                File fdelete = new File(path);
                if (fdelete.exists()) {
                    if (fdelete.delete()) {
                        System.out.println("file Deleted :" + path);
                        Toast.makeText(context, "File Deleted", Toast.LENGTH_SHORT).show();
                        imgid.remove(i);

                        notifyDataSetChanged();
                    } else {
                        Toast.makeText(context, "File not deleted", Toast.LENGTH_SHORT).show();
                    }
                }



            }
        });

        viewHolder.share_video_layout.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                CommonMethods.shareMediaThroughIntent("video/*", (String) MediaAdapter.imgid.get(i), 1, MediaAdapter.context);
            }
        });
        return view2;
    }


//    public static void fetchingMediaFromDirectory() {
//        File[] listFiles = new File(CommonMethods.pathName1).listFiles();
//        if (listFiles != null) {
//            for (File file : listFiles) {
//                if (file.isFile()) {
//                    ArrayList<String> arrayList = imgid;
//                    StringBuilder sb = new StringBuilder();
//                    sb.append(CommonMethods.pathName1);
//                    sb.append(file.getName());
//                    arrayList.add(sb.toString());
//                }
//            }
//        }
//        if (imgid.size() == 0) {
//            noMedia.setVisibility(View.VISIBLE);
//            return;
//        }
//        Collections.sort(results);
//        noMedia.setVisibility(View.GONE);
//        mediaAdapter.notifyDataSetChanged();
//    }
}
