package com.faysal.tiktok.ClipboardService;

import android.app.Service;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.faysal.tiktok.R;
import com.faysal.tiktok.myapp.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;



/**
 * Created by Faysal Ahmed Shakil on 12/9/2017.
 */

public class AutoDirectService extends Service {

    ClipboardManager clipboard;
    String defultClip = "*TikTok Downloader*";
    String nullText;
    private Timer timer;



    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        clipboard = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setText("");
        nullText = "";

        TimerTask local1 = new TimerTask()
        {
            public void run()
            {
                if ((clipboard.hasText()) && (!clipboard.getText().toString().contentEquals(nullText))) {
                    try
                    {
                        if (!clipboard.getText().toString().contains(defultClip))
                        {

                            String url=clipboard.getText().toString();
                            nullText = clipboard.getText().toString();

                            Log.d("GFDSGDFG", "run: "+url);

                            if (pasteDataFromURL(url)){
                                Intent launchIntentForPackage = getPackageManager().getLaunchIntentForPackage("com.faysal.tiktok");
                                if (launchIntentForPackage != null) {
                                    launchIntentForPackage.putExtra("url",url);
                                    startActivity(launchIntentForPackage);
                                } else {
                                    //Toast.makeText(getApplicationContext(), getString(R.string.str_tiktok_not_installed), Toast.LENGTH_SHORT).show();
                                }

                            }


                            return;
                        }
                        nullText = clipboard.getText().toString().replace(defultClip, "");
                        clipboard.setText(nullText);
                        return;
                    }
                    catch (Exception localSQLiteException)
                    {
                        localSQLiteException.printStackTrace();
                    }
                }
            }
        };
        this.timer = new Timer();
        this.timer.schedule(local1, 0L, 1000L);


        return START_NOT_STICKY;
    }

    private boolean pasteDataFromURL(String obj) {
        if (obj.toLowerCase().contains("tiktok") || obj.toLowerCase().contains("musical.ly")) {

            return true;
        }
        return  false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
        timer=null;
        stopSelf();
    }



}
