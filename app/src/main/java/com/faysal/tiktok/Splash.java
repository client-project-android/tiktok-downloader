package com.faysal.tiktok;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.faysal.tiktok.helper.SharedHelper;
import com.faysal.tiktok.myapp.MainActivity;



/*
 * Created by Faysal Ahmed Shakil
 * Software Engineer @APPBD
 */


public class Splash extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Intent intent=getIntent();

            int SPLASH_TIME_OUT = 1300;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (SharedHelper.getKey(getApplicationContext(),"first_launch").equals("false")){
                        Intent intent = new Intent(Splash.this, ActivityGDPR.class);
                        startActivity(intent);
                        finish();
                    }else {
                        Intent intent = new Intent(Splash.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }

                }
            }, SPLASH_TIME_OUT);
        }



}
