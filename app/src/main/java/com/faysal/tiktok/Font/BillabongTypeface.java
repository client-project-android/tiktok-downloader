package com.faysal.tiktok.Font;

import android.content.Context;
import android.graphics.Typeface;

public class BillabongTypeface {
    public static Typeface instance;

    public static Typeface getInstance(Context context) {
        Typeface typeface = instance;
        if (typeface != null) {
            return typeface;
        }
        instance = Typeface.createFromAsset(context.getAssets(), "fonts/billabong.ttf");
        return instance;
    }
}
