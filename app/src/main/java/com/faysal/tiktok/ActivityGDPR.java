package com.faysal.tiktok;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.faysal.tiktok.myapp.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityGDPR extends AppCompatActivity {

    @BindView(R.id.sv_text_first)
    ScrollView sv_text_first;

    @BindView(R.id.sv_text_second)
    LinearLayout sv_text_second;

    @BindView(R.id.tv_yes)
    TextView tv_yes;

    @BindView(R.id.tv_no)
    TextView tv_no;

    @BindView(R.id.ll_button_close)
    LinearLayout ll_button_close;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gdpr);
        ButterKnife.bind(this);

        tv_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sv_text_first.setVisibility(View.GONE);
                sv_text_second.setVisibility(View.VISIBLE);
            }
        });

        tv_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sv_text_first.setVisibility(View.GONE);
                sv_text_second.setVisibility(View.VISIBLE);
            }
        });

        ll_button_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ActivityHTU.class));
                finish();
            }
        });


    }

}
