package com.faysal.tiktok.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.faysal.tiktok.R;
import com.faysal.tiktok.myapp.MainActivity;

import butterknife.BindView;


public class FragmentHowToUse extends Fragment {


    View view;
    Button btnokunderstand;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view =inflater.inflate(R.layout.fragment_howtouse,container,false);

        btnokunderstand=view.findViewById(R.id.btnokunderstand);
        btnokunderstand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).setPagerItem(0);
            }
        });

        return view;
    }
}
