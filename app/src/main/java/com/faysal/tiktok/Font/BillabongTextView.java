package com.faysal.tiktok.Font;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class BillabongTextView extends AppCompatTextView {
    public BillabongTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setTypeface(BillabongTypeface.getInstance(context));
    }
}
