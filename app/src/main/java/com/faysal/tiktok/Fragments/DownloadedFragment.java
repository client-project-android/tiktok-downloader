package com.faysal.tiktok.Fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.faysal.tiktok.Adapters.MediaAdapter;
import com.faysal.tiktok.R;
import com.faysal.tiktok.Utils.CommonMethods;
import com.google.android.gms.ads.AdRequest;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class DownloadedFragment extends Fragment {
    private static Activity activity;
    public static ImageView delete_btn;
    public static MediaAdapter mediaAdapter;
    public static LinearLayout noMedia;
    public static GridView photos_grid;
    public static ArrayList<String> results = new ArrayList<>();
    /* access modifiers changed from: private */
    public static boolean setMultipleSelection = false;
    private TextView no_media;
    View rootview;
    SharedPreferences sharedPreferences;
    private AdView adView;



    public View onCreateView(@NonNull LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.rootview = layoutInflater.inflate(R.layout.downloaded, viewGroup, false);
        activity = getActivity();
        this.sharedPreferences = activity.getSharedPreferences("musically", 0);
        CommonMethods.is_a_premium_member = Boolean.valueOf(this.sharedPreferences.getBoolean("isapremiummember", false));
        setMultipleSelection = false;
        this.no_media = (TextView) this.rootview.findViewById(R.id.no_media);
        noMedia = (LinearLayout) this.rootview.findViewById(R.id.noMedia);
        photos_grid = (GridView) this.rootview.findViewById(R.id.photos_grid);
        mediaAdapter = new MediaAdapter(activity, results);
        photos_grid.setAdapter(mediaAdapter);
      //  this.no_media.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/billabong.ttf"));
        fetchingMediaFromDirectory();

        initAdmobAds();
        return this.rootview;
    }

    private void initAdmobAds() {
        adView = new AdView(getActivity(), getResources().getString(R.string.banner_placement_id), AdSize.BANNER_HEIGHT_50);

        // Find the Ad Container
        LinearLayout adContainer = (LinearLayout)rootview.findViewById(R.id.banner_container);

        // Add the ad view to your activity layout
        adContainer.addView(adView);

        // Request an ad
        adView.loadAd();
    }

    /* access modifiers changed from: private */
    public void deleteFiles() {
        while (CommonMethods.selectedPosition.size() > 0) {
            File file = new File((String) results.get(((Integer) CommonMethods.selectedPosition.get(0)).intValue()));
            CommonMethods.selectedPosition.remove(0);
            if (file.exists()) {
                file.delete();
            }
        }
        fetchingMediaFromDirectory();
    }

    public static void fetchingMediaFromDirectory() {
        results.clear();
        File[] listFiles = new File(CommonMethods.pathName1).listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (file.isFile()) {
                    ArrayList<String> arrayList = results;
                    StringBuilder sb = new StringBuilder();
                    sb.append(CommonMethods.pathName1);
                    sb.append(file.getName());
                    arrayList.add(sb.toString());
                }
            }
        }
        if (results.size() == 0) {
            noMedia.setVisibility(View.VISIBLE);
            return;
        }
        Collections.sort(results);
        noMedia.setVisibility(View.GONE);
        mediaAdapter.notifyDataSetChanged();
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
